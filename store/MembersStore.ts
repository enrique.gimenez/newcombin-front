import { createStore } from 'redux'
import { membersReducer } from '../reducers/membersReducer'

export const MembersStore = createStore(membersReducer)
