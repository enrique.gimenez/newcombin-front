import { createContext } from 'react'

import { authInitialState } from '../utils/constants'

export const AuthContext = createContext(authInitialState)
