import { createContext } from 'react'

import { membersInitialState } from '../reducers/membersReducer'

export const MembersContext = createContext(membersInitialState)
