export interface Member {
  firstName: string
  lastName: string
  address: string
  ssn: string
}

export type MembersActionType = {
  type: 'SET'
  payload: string
}
