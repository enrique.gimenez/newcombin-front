export type AuthActionType = {
  type: 'SET'
  payload: {
    token: string
  }
}

export interface AuthInitialStateType {
  token: string
}
