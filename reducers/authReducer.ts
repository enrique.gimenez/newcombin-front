import { AuthActionType } from '../types/auth'
import { authInitialState } from '../utils/constants'

export const authReducer = (
  state = authInitialState,
  action: AuthActionType
) => {
  switch (action.type) {
    case 'SET':
      return {
        ...state,
        token: action.payload.token,
      }

    default:
      return state
  }
}
