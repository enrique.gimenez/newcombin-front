import { MembersActionType } from '../types/members'

export const membersInitialState = {
  lastChange: '',
}
type InitialState = {
  lastChange: string
}
export const membersReducer = (
  state: InitialState = membersInitialState,
  action: MembersActionType
) => {
  switch (action.type) {
    case 'SET':
      return {
        ...state,
        lastChange: action.payload,
      }
    default:
      return state
  }
}
