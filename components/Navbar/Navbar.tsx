import React from 'react'

import styles from './Navbar.module.css'

const MainNavbar = () => {
  const { navContainer, home, other } = styles
  return (
    <nav className={navContainer}>
      <div className={home}>Home</div>
      <div className={other}>Other Page</div>
    </nav>
  )
}

export default MainNavbar
