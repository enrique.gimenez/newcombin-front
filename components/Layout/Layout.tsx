import React, { FC, useEffect } from 'react'
import { Container } from 'react-bootstrap'

import Footer from '../Footer/Footer'
import MainNavbar from '../Navbar/Navbar'

const Layout: FC = ({ children }) => {
  useEffect(() => {
    restartAutoReset()
  }, [])
  const restartAutoReset = () => {
    let timeout: NodeJS.Timeout | null = null
    if (timeout) {
      clearTimeout(timeout)
    }
    timeout = setTimeout(() => {
      // Insert your code to reset you app here
    }, 1000 * 60 * 5) // 60 Seconds
  }

  return (
    <Container fluid onScroll={restartAutoReset} onMouseMove={restartAutoReset}>
      <MainNavbar />
      {children}
      <Footer />
    </Container>
  )
}

export default Layout
