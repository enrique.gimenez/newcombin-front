import React from 'react'

import styles from './Footer.module.css'

const Footer = () => {
  return (
    <div className={styles.footer}>
      <div className={styles.box}>copyright</div>
      <div className={styles.box}>All rights reserved</div>
    </div>
  )
}

export default Footer
