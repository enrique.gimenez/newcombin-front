import React from 'react'

import styles from './SubmitButton.module.css'

interface Props {
  text: string
  action: () => void
  disabled: boolean
}

const SubmitButton = ({ text, action, disabled }: Props) => {
  return (
    <button
      onClick={action}
      className={styles.button}
      disabled={disabled}
      style={{
        cursor: disabled ? 'not-allowed' : 'pointer',
      }}
    >
      {text}
    </button>
  )
}

export default SubmitButton
