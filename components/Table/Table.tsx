import React, { useEffect, useState } from 'react'
import { Table } from 'react-bootstrap'

import styles from './Table.module.css'

const CustomTable = ({ lastChange }: { lastChange: string }) => {
  const [members, setMembers] = useState([])

  useEffect(() => {
    const members = localStorage.getItem('members')
    setMembers(JSON.parse(members as string))
  }, [lastChange])
  return (
    <div className={styles.table}>
      <Table striped bordered hover>
        <thead>
          <tr>
            {['First Name', 'Last Name', 'Address', 'SSN'].map(
              (head, index) => {
                return <th key={index}>{head}</th>
              }
            )}
          </tr>
        </thead>
        <tbody>
          {members &&
            members.length > 0 &&
            members.map((member: any, index: number) => {
              const { firstName, lastName, address, ssn } = member
              return (
                <tr key={index}>
                  <td>{firstName}</td>
                  <td>{lastName}</td>
                  <td>{address}</td>
                  <td>{ssn}</td>
                </tr>
              )
            })}
        </tbody>
      </Table>
    </div>
  )
}

export default CustomTable
