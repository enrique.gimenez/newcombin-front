import React, { useState } from 'react'
import ContentForm from '../Form/Form'
import CustomTable from '../Table/Table'

import styles from './Content.module.css'

const Content = () => {
  const [lastChange, setLastChange] = useState('')
  const { content, form, table } = styles
  return (
    <div className={content}>
      <div className={form}>
        <ContentForm change={setLastChange} />
      </div>
      <div className={table}>
        <CustomTable lastChange={lastChange} />
      </div>
    </div>
  )
}

export default Content
