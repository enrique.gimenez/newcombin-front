import React, { SetStateAction } from 'react'
import { Form } from 'react-bootstrap'

import styles from './CustomInput.module.css'

interface Props {
  label: string
  value: string | number
  onValueChange: (args: string) => void
  isSSN?: boolean
  placeholder?: string
}

const CustomInput = ({
  label,
  value,
  onValueChange,
  isSSN = false,
  placeholder = '',
}: Props) => {
  const { Group, Control, Label } = Form
  return (
    <Group>
      <Label>{label}</Label>
      <Control
        value={value}
        type='string'
        onChange={e => onValueChange(e.target.value)}
        className={styles.input}
        maxLength={isSSN ? 11 : 524288}
        placeholder={placeholder}
      />
    </Group>
  )
}

export default CustomInput
