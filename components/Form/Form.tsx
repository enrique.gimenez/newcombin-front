import React, { useState } from 'react'
import { Form } from 'react-bootstrap'
import { MembersStore } from '../../store/MembersStore'
import api from '../../utils/api'
import SubmitButton from '../Buttons/SubmitButton'
import CustomInput from '../Input/CustomInput'

import styles from './Form.module.css'
import { notify } from '../../utils/notify'

const ContentForm = ({ change }: { change: (args: string) => void }) => {
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [address, setAddress] = useState('')
  const [SSN, setSSN] = useState('')

  const reset = () => {
    setFirstName('')
    setLastName('')
    setAddress('')
    setSSN('')
  }

  const submit = async () => {
    try {
      const response = await api.post(
        '/api/members',
        {
          firstName,
          lastName,
          address,
          ssn: SSN,
        },
        {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
        }
      )
      const data = await response.data

      MembersStore.dispatch({
        type: 'SET',
        payload: new Date().toString(),
      })

      localStorage.setItem(
        'members',
        JSON.stringify([
          ...JSON.parse(localStorage.getItem('members') as string),
          data,
        ])
      )
      localStorage.setItem('lastChange', new Date().toString())
      change(new Date().toString())

      setSSN('')
      setAddress('')
      setFirstName('')
      setLastName('')
      notify({ type: 'success' })
    } catch (error) {
      notify({ type: 'error', message: 'Bad request' })
      console.error(error)
    }
  }

  const inputs = [
    {
      label: 'First Name',
      value: firstName,
      onValueChange: setFirstName,
    },
    {
      label: 'Last Name',
      value: lastName,
      onValueChange: setLastName,
    },
    {
      label: 'Address',
      value: address,
      onValueChange: setAddress,
    },
    {
      label: 'SSN',
      value: SSN,
      onValueChange: setSSN,
      placeholder: 'e.g.: 111-22-3333',
    },
  ]

  const regex = RegExp(/^[0-9][0-9][0-9][-][0-9][0-9][-][0-9][0-9][0-9][0-9]$/g)

  const disableSave =
    regex.test(SSN) &&
    firstName.length > 1 &&
    lastName.length > 1 &&
    address.length > 1

  return (
    <Form className={styles.customForm} onSubmit={e => e.preventDefault()}>
      {inputs.map((input, index) => {
        const { label, onValueChange, value, placeholder } = input
        return (
          <CustomInput
            key={index}
            label={label}
            onValueChange={e => onValueChange(e)}
            value={value}
            isSSN={input.label === 'SSN'}
            placeholder={placeholder ? placeholder : ''}
          />
        )
      })}
      <div className={styles.buttons}>
        <SubmitButton text='Reset' action={reset} disabled={false} />
        <SubmitButton
          text='Save '
          action={async () => submit()}
          disabled={!disableSave}
        />
      </div>
    </Form>
  )
}

export default ContentForm
