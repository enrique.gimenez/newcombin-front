import axios from 'axios'
import { authStore } from '../store/AuthStore'

const getToken = authStore.getState().token

export default axios.create({
  baseURL:
    process.env.NEXT_PUBLIC_ENVIRONMENT === 'dev'
      ? 'http://localhost:8081'
      : 'https://starwarsapi.com',
})
