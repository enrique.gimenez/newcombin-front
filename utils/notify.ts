import { toast } from 'react-toastify'

type Type = 'error' | 'success'

interface Props {
  message?: string
  type: Type
}

export const notify = ({ message = 'Created successfully', type }: Props) => {
  toast(message, {
    type,
  })
}
