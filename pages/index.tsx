import { useEffect } from 'react'

import Content from '../components/Content/Content'
import { Member } from '../types/members'
import api from '../utils/api'
import { credentials } from '../utils/constants'
import { authStore } from '../store/AuthStore'

const Home = ({ members, token }: { members: Member[]; token: string }) => {
  useEffect(() => {
    localStorage.setItem('members', JSON.stringify(members))
    localStorage.setItem('token', token)
  })
  return <Content />
}
export default Home

export async function getServerSideProps() {
  const authResponse = await api.post('/auth', credentials)
  const data = await authResponse.data
  authStore.dispatch({ type: 'SET', payload: data.token })
  console.log(data.token)
  const membersResponse = await api.get('/api/members', {
    headers: {
      Authorization: 'Bearer ' + data.token,
    },
  })
  const members = membersResponse.data
  console.log(members)

  return {
    props: {
      members,
      token: data.token,
    },
  }
}
