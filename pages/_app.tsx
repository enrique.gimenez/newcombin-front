import type { AppProps } from 'next/app'
import { ToastContainer } from 'react-toastify'
import 'bootstrap/dist/css/bootstrap.css'
import 'react-toastify/dist/ReactToastify.css'

import Layout from '../components/Layout/Layout'
import { AuthProvider } from '../providers/AuthProvider'
import MembersProvider from '../providers/MembersProvider'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <MembersProvider>
      <AuthProvider>
        <Layout>
          <Component {...pageProps} />
          <ToastContainer
            position='top-right'
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
          />
          {/* Same as */}
          <ToastContainer />
        </Layout>
      </AuthProvider>
    </MembersProvider>
  )
}

export default MyApp
