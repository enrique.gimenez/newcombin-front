import { FC } from 'react'

import { AuthContext } from '../context/AuthContext'
import { authInitialState } from '../utils/constants'

export const AuthProvider: FC = ({ children }) => {
  return (
    <AuthContext.Provider value={authInitialState}>
      {children}
    </AuthContext.Provider>
  )
}
