import React, { FC } from 'react'

import { MembersContext } from '../context/MembersContext'
import { membersInitialState } from '../reducers/membersReducer'

const MembersProvider: FC = ({ children }) => {
  return (
    <MembersContext.Provider value={membersInitialState}>
      {children}
    </MembersContext.Provider>
  )
}

export default MembersProvider
